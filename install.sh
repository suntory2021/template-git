git clone https://gitlab.com/hawkingtools/template-git.git

cp template-git/commit_msg_template.txt .git/commit_msg_template.txt
cp template-git/commit-msg .git/hooks/commit-msg
chmod +x .git/hooks/commit-msg

currentTemplate=$(git config --get commit.template)
currentCleanup=$(git config --get commit.cleanup)
if [ $currentTemplate != ".git/commit_msg_template.txt" ]
then
  git config --add commit.template .git/commit_msg_template.txt
fi
if [ $currentCleanup != "strip" ]
then
  git config --add commit.cleanup strip
fi

rm -rf template-git
